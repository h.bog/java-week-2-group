package no.experis.academy;

import java.io.*;
import java.util.Stack;

public class Main {

    public static void main(String[] args) {

        Main main = new Main();
        String content = main.readFileString();

        //Checking syntax of file content, '()', '[]' and '{}' exclusively
        if (main.checkSyntax(content)){
            System.out.println("Balanced!");
        } else{
            System.out.println("Not balanced");
        }
    }

    //Returns true if brackets are balanced
    public boolean checkSyntax(String content){
        Stack<Character> stack = new Stack<>();

        for(char ch: content.toCharArray()) {
            if (!stack.empty() && ch == invertChar(stack.peek())) {
                //Popping stack if ch is the closing bracket of the top element of the stack
                stack.pop();
            } else if (ch == '(' || ch == '{' || ch == '[') {
                //Pushing ch onto the stack if it is an opening bracket
                stack.push(ch);
            } else {
                //Returning false if ch is not the closing bracket of the top element or an opening bracket
                return false;
            }
        }
        // Return false if the stack is not empty when all characters have been traversed without reaching
        // the above return statement, returning true if the stack is empty (all brackets have been closed properly)
        return stack.empty();
    }
    //Returns closing bracket
    public Character invertChar(Character c){
        if(c == '('){
            return ')';
        }else if (c == '['){
            return ']';
        }else if (c == '{'){
            return '}';
        }else{
            throw new IllegalArgumentException();
        }
    }

    //Read file and return only brackets
    public String readFileString(){
        String content = "";

        try {
            File file = new File("resources/file.java");
            String line;

            BufferedReader reader = new BufferedReader(new FileReader(file));

            while ((line = reader.readLine()) != null){
                //Appends all braces to content string
                for(char ch: line.toCharArray()){
                    if(ch == '(' || ch == ')' || ch == '[' || ch == ']' || ch == '{' || ch == '}'){
                        content+=ch;
                    }
                }
            }
        } catch (IOException e){
            System.out.println(e.getMessage());
            System.exit(0);
        }
        return content;
    }
}
