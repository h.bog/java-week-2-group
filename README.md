# Group Ann Marlene

## Group Members
Helene Boge,
Ann Katrin Bråten and
Martin Skurtveit

## Our version of pair programming, "Trair programming"
As the team consisted of three and not two java fullstack developers, they decided to do trair programming.
This is identical to pair programming, except that there is one extra person assisting. Trair programming proved to be challenging,
but manageable. By putting up coding intervals of approximately 20 minutes per person, they got to test each others patience.
Frustration soon developed, but after hours of struggling to get the cleanest code, the team decided it was done. Delivered. Thank you.