
import java.sql.*;

public class file{
    public static void main(String[] args) {
        String query;
        try {
            Connection conn = connect();
            Statement s = conn.createStatement();
            query = "SELECT * FROM Customer WHERE Country = 'Mexico' AND ContactName = 'Antonio Moreno'";
            ResultSet rs = s.executeQuery(query);
            System.out.println("\nQuery: " + query);
            while (rs.next()){
                System.out.println(rs.getString("ContactName") +" from "+ rs.getString("country"));
            }
            query = "SELECT * FROM employee";
            System.out.println("\nQuery: " + query);
            rs = s.executeQuery(query);

            String format = "%-30s%s%n";
            System.out.printf(format, "Name:", "Title:");
            while (rs.next()){
                System.out.printf(  format,
                        rs.getString("firstname")+" "+rs.getString("lastname"),
                        rs.getString("title"));
            }
            disconnect(conn);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void disconnect(Connection conn){
        try {
            if (conn != null){
                conn.close();
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static Connection connect() {
        System.out.println("Connecting to SQLite ....");
        Connection conn = null;
        try {
            String url = "jdbc:sqlite:resources/Northwind_small.sqlite";
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been established");
         }catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }
}
